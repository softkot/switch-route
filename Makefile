.PHONY: sudo
sudo:
	cp sudo-config /etc/sudoers.d/switch-route
	rm sudo-config

.PHONY: install
install: binary
	echo "${USER}  ALL=NOPASSWD:  /usr/sbin/ip route *" > sudo-config
	install -s route-switch ${HOME}/bin/
	killall -q route-switch || true
	nohup route-switch --route="172.24.194.209 Россия" --route="172.24.91.201 Германия" </dev/null >/dev/null &

.PHONY: binary
binary:
	go build -ldflags "-s -w"
.PHONY: proto
proto:
	rm -rf pb
	mkdir -p pb
	protoc -I proto \
		--go-grpc_opt=require_unimplemented_servers=false,paths=source_relative  --go-grpc_out=pb \
		--go_opt=paths=source_relative  --go_out=pb $(shell find proto -type f -name '*.proto' -printf '%P\n')
