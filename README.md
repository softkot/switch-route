# route-switch tray icon

Switch default routes provided as argument
## Configuration

Has command line arguments

* *--route* - set a menu route

## Setup instructions:

The simple way to set it up with the default configuration by running following commands in terminal:

    $ git clone https://gitlab.com/softkot/switch-route.git
    
    $ cd switch-route
    
edit switch-route.service ExecStart line to add proper --route args

    $ make install && sudo make sudo


