//go:generate env CGO_ENABLED=0 go build -ldflags "-s -w"
package main

import (
	"bufio"
	"bytes"
	"context"
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/driver/desktop"
	"github.com/emersion/go-autostart"
)

type RouteItem struct {
	Gateway  string
	Label    string
	MenuItem *fyne.MenuItem
}
type routes []*RouteItem

func (r *routes) String() string {
	return fmt.Sprintf("%v", *r)
}

func (r *routes) Set(s string) error {
	parts := strings.Split(s, " ")
	rt := &RouteItem{
		Gateway: parts[0],
	}
	rt.Label = parts[len(parts)-1]

	*r = append(*r, rt) //TODO implement me
	return nil
}

var rtlist routes
var ctx, cancel = signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGINT, syscall.SIGTERM)
var desk desktop.App
var menu *fyne.Menu
var appication fyne.App

func main() {
	defer cancel()
	rtlist = append(rtlist, &RouteItem{
		Gateway: "",
		Label:   "Не активно",
	})
	flag.Var(&rtlist, "route", "route item")
	flag.Parse()
	appstart := &autostart.App{
		Name:        "switch-route",
		DisplayName: "VPN",
		Exec:        os.Args,
	}
	if len(appstart.Exec) > 1 {
		appstart.Enable()
	}
	appication = app.NewWithID("switch-route")
	appication.Settings().ShowAnimations()
	if d, ok := appication.(desktop.App); ok {
		desk = d
		onReady()
	}
	go func() {
		<-ctx.Done()
		appication.Quit()
	}()
	for ctx.Err() == nil {
		appication.Run()
	}
	log.Println("Quit")
}

//go:embed icon.png
var icon []byte

//go:embed inactive.png
var inactive []byte

//go:embed dot.png
var dot []byte

func onReady() {
	log.Println("Ready")
	var mitems []*fyne.MenuItem
	// systray.SetTitle("VPN")
	// systray.SetTooltip("Set routes")
	// systray.SetIcon(icon)

	for _, r := range rtlist {
		r.MenuItem = fyne.NewMenuItem(r.Label, func() {
			selectRoute(r)
		})
		r.MenuItem.IsQuit = true
		mitems = append(mitems, r.MenuItem)
	}
	menu = fyne.NewMenu("VPN", mitems...)

	desk.SetSystemTrayMenu(menu)
	desk.SetSystemTrayIcon(fyne.NewStaticResource("appicon", icon))
	selectRoute(nil)
}

func runcmd(args ...string) []string {
	var res []string
	if len(args) > 0 {
		cmd := args[0]
		args = args[1:]
		var out []byte
		if reader, err := exec.Command(cmd, args...).CombinedOutput(); err != nil {
			log.Printf("Unable to exec %v %v\n%v", args, err, string(reader))
		} else {
			out = reader
		}
		if out != nil {
			scanner := bufio.NewScanner(bytes.NewBuffer(out))
			for scanner.Scan() {
				res = append(res, scanner.Text())
			}
		}
	}
	return res
}
func getDefaultRoute() *RouteItem {
	var defaults = make(map[string]string)
	for _, ri := range runcmd("ip", "route", "list", "default") {
		parts := strings.Split(ri, " ")
		if len(parts) > 5 {
			defaults[parts[2]] = parts[2]
		}
	}
	for _, r := range rtlist {
		if _, ok := defaults[r.Gateway]; ok {
			return r
		}
	}
	return nil
}
func selectRoute(r *RouteItem) {
	selected := getDefaultRoute()
	if r != nil {
		bcmd := ""
		if selected != nil {
			bcmd += "ip route delete default via " + selected.Gateway
			runcmd("sudo", "ip", "route", "delete", "default", "via", selected.Gateway)
		}
		if r.Gateway != "" {
			if bcmd != "" {
				bcmd += " && "
			}
			bcmd += "ip route add default via " + r.Gateway
			runcmd("sudo", "ip", "route", "add", "default", "via", r.Gateway)
		}
		if bcmd != "" {
			//runcmd("pkexec", "sh", "-c", bcmd)
			selected = getDefaultRoute()
		}
	}
	if selected == nil {
		selected = rtlist[0]
	}
	appication.SendNotification(fyne.NewNotification("VPN route", selected.Label))
	// runcmd("notify-send", "VPN route", selected.Label)
	for _, item := range rtlist {
		if item == selected {
			item.MenuItem.Checked = true
			// item.MenuItem.SetTitle("✅ " + item.Label)
		} else {
			item.MenuItem.Checked = false
		}
	}
	if menu != nil {
		menu.Refresh()
	}
}

func onExit() {
	cancel()
}
